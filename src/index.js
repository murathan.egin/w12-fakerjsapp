import React from "react";
import { render } from "react-dom";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import faker from "faker";



const Faker = () => {
  return (
    <>     
      { <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          height: "80vh",
        }}
      >
        <form>
          <TextField
            label="First Name"
            variant="outlined"
            defaultValue={faker.name.firstName()}
          />
          <TextField
            label="Last Name"
            variant="outlined"
            defaultValue={faker.name.lastName()}
          />
          <br />
          <br />
          <TextField
            label="Street Address"
            variant="outlined"
            defaultValue={faker.address.streetAddress()}
          />
          <TextField
            label="Country"
            variant="outlined"
            defaultValue={faker.address.country()}
          />
          <br />
          <br />
          <TextField
            label="Job Title"
            variant="outlined"
            defaultValue={faker.name.jobTitle()}
          />
          <TextField
            label="Job Area"
            variant="outlined"
            defaultValue={faker.name.jobTitle()}
          />
          <br />
          <br />
          <TextField
            label="Phone Number"
            variant="outlined"
            defaultValue={faker.phone.phoneNumber()}
          />
          <TextField
            label="Email"
            variant="outlined"
            defaultValue={faker.internet.email()}
          />
          <br />
          <br />
          <Button variant="contained" color="primary" onClick={() => {}}>
            Generate
          </Button>
          <br />
        </form>
      </div> 
    }
    </>
  );
};

render(<Faker />, document.querySelector("#root"));